#!/bin/bash

echo "Content-type: text/html"
echo


# Jim.Perry.Jr
# CS6261: System and Network Administration
# Card Dealer with CGI-BIN
# live version here: http://13.68.209.70/cgi-bin/bashjack.sh

sPace='&nbsp;&nbsp;&nbsp;&nbsp;'
suite=('matches group number in order'  '&hearts;' '&spades;' '&diams;' '&clubs;')

group1=('A' '2' '3' '4' '5' '6' '7' '8' '9' '10' 'J' 'Q' 'K')
group2=('A' '2' '3' '4' '5' '6' '7' '8' '9' '10' 'J' 'Q' 'K')
group3=('A' '2' '3' '4' '5' '6' '7' '8' '9' '10' 'J' 'Q' 'K')
group4=('A' '2' '3' '4' '5' '6' '7' '8' '9' '10' 'J' 'Q' 'K')

value=(11 2 3 4 5 6 7 8 9 10 10 10 10)
tValue=0


for ((link=0; link < 9; link++))
        do
        echo '<font size="5">'  '<a href=/cgi-bin/bashjack.sh?'$(($link+1))'>Draw ' $(($link+1)) '</a> </font> &nbsp;&nbsp;'
done
echo '<br>'


if [[ $QUERY_STRING ==  "" ]]; then
        for ((i=1; i <10 ; i++))

                do

                a=$(($i))
                b=$[ ($RANDOM % 13)]
                c=$[ ($RANDOM % 4 ) +1 ]

                $((tValue+=${value[$b]}))
                cardGroup=group$c[$b]

                echo 'card ' $a   $sPace ' <font size="8">'${!cardGroup}'</font>'   '<font size="8">' ${suite[$c]}'</font>' $sPace  ${value[$b]}   'points <br>'
        done
        echo '<br>'
        echo '<font size="4">' 'Default of 9 cards  totaling  [' $tValue  '] points.' '</font>'

else
        if [[ $QUERY_STRING -lt 10 ]] && [[ $QUERY_STRING =~ ^[0-9]+$ ]] ; then
                for ((i=1; i < $(($QUERY_STRING+1)); i++))
                        do

                        a=$(($i))
                        b=$[ ($RANDOM % 13)]
                        c=$[ ($RANDOM % 4 ) + 1]

                        $((tValue+=${value[$b]}))
                        cardGroup=group$c[$b]

                        echo 'card ' $a   $sPace ' <font size="8">'${!cardGroup}'</font>'   '<font size="8">' ${suite[$c]}'</font>' $sPace  ${value[$b]}   'points <br>'
                done
        else
                echo '<br><br>'
                echo 'A maximun of 9 cards allowed.<br>'
                echo 'Please edit URL so that only intergers 0-9<br> '
                echo 'are visible. No characters after the ? in URL.'

       fi
        echo '<br>'
        echo '<font size="4">[ ' $QUERY_STRING ' ] cards requested  totaling ' $tValue  ' points.' '</font>'

fi

echo '<br><br><br>'
echo ' <font size="5"> <a href="./bashjack.sh"> HOME  </a> </font><br>'
echo 'resets to default number of cards'